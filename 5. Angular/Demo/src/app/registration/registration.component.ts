// import { Component } from '@angular/core';

// @Component({
//   selector: 'app-registration',
//   templateUrl: './registration.component.html',
//   styleUrl: './registration.component.css'
// })
// export class RegistrationComponent {

//   empName: string = '';
//   salary: number = 0;
//   gender: string = '';
//   doj: string = '';
//   country: string = '';
//   emailId: string = '';
//   password: string = '';
//   deptId: number = 0;

//   register() {
//     if (!this.empName || !this.salary || !this.gender || !this.doj || !this.country || !this.emailId || !this.password || !this.deptId) {
//       alert('Please fill in all fields.');
//     } else if (!this.validateEmail(this.emailId)) {
//       alert('Please enter a valid email address.');
//     } else {
     
//       alert('Registration successful');
//     }
//   }

 
//   validateEmail(email: string): boolean {
//     const pattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
//     return pattern.test(email);
//   }
// }


import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  emp: any;
  countries: any;
  departments: any;

  constructor(private router: Router, private service: EmpService) {

    //Making the emp object in such a way that it contains the department json object
    this.emp = {
      empId: '',
      empName: '',
      salary: '',
      gender: '',
      doj: '',
      country: '',
      emailId: '',
      password: '',
      department: {
        deptId:''
      }
    }
  }

  ngOnInit() {
    this.service.getAllCountries().subscribe((data: any) => { this.countries = data; });
    this.service.getAllDepartments().subscribe((data: any) => { 
      console.log(data);
      this.departments = data; 
    });
  }

  empRegistration(regForm: any) {

    //Binding the registerForm data to the emp object, as it contains the department as a Json object
    this.emp.empName = regForm.empName;
    this.emp.salary = regForm.salary;
    this.emp.gender = regForm.gender;
    this.emp.doj = regForm.doj;
    this.emp.country = regForm.country;
    this.emp.emailId = regForm.emailId;
    this.emp.password = regForm.password;
    this.emp.department.deptId = regForm.department;   
    
    console.log(regForm);
    console.log(this.emp);

    this.service.registerEmplooyee(this.emp).subscribe((data: any) => { console.log(data); });

    alert("Employee Registered Successfully!!!");
    this.router.navigate(['login']);
  }
}